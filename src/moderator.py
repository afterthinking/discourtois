# libraries
import discord
import datetime
import os
import re

import log
import embed
import database as db

from dotenv import load_dotenv

load_dotenv()

blacklist_file=str(os.getenv("BLACKLIST"))
CHANNEL_LOG=int(os.getenv("CHANNEL_LOG"))



blacklist = None


"""
"   Load the words from blacklist database into an array.
"   @args : none 
"   @return : nothing
"""
def load_blacklist():
    global blacklist
    blacklist = []

    # if database file doesn't exist, create the DB
    if(not os.path.exists(blacklist_file)):
        log.log("Database don't exists. Creating it...")
        db.db_create()
        log.log("Done.")

    # append word list to the array used later
    bList = list(db.blacklist_db_get())
    for line in bList:
        blacklist.append(line[0])



"""
"   Display the list of the words in the blacklist
"   @arg1 : object - discord object to use the discord API
"   @return : nothing
"""
async def blacklist_display(bot):
    admin_channel = bot.get_channel(CHANNEL_LOG)
    await admin_channel.send("Voici la liste des mots interdit sur le serveur :")
    await admin_channel.send(sorted(blacklist))
 

"""
"   Add a word or multiple words into the blacklist
"   @arg1 : object - discord object to use the discord API
"   @arg3 : object - message object from the discord API
"   @return : nothing
"""
async def blacklist_add(bot, message):
    log.log("Adding words to blacklist")
    admin_channel = bot.get_channel(CHANNEL_LOG)

    new_words = message.split()
    new_words.pop(0) # remove bot mention
    new_words.pop(0) # remove add keyword
    if(new_words): 
        for word in new_words:
            if word not in blacklist:
                chars = ['\.', '\,', '\?', '\;', '\:', '\/', '\§', '\!', '\'', '\"', '\<', '\>', '\~', '\*', '\`', '_']
                word = re.sub("|".join(chars), "", word)

                blacklist.append(word)
                await admin_channel.send("Le mot « " + word + " » est désormais censuré sur le serveur")
                await db.blacklist_db_add(bot, word)
            else:
                await admin_channel.send("Le mot « " + word + " » est déjà dans la liste noire.")
    else:
        await admin_channel.send("Une demande d'ajout de mot à la liste noire a été faite mais aucun mot n'a été spécifié.")



"""
"   Remove a word or multiple words into the blacklist
"   @arg1 : object - discord object to use the discord API
"   @arg2 : object - message object from the discord API
"   @return : nothing
"""
async def blacklist_rm(bot, message):
    log.log("Removing words of blacklist")
    admin_channel = bot.get_channel(CHANNEL_LOG)

    new_words = message.split()
    new_words.pop(0) # remove bot mention
    new_words.pop(0) # remove rm keyword
    if(new_words): 
        for word in new_words:
            if word in blacklist:
                blacklist.remove(word)
                await db.blacklist_db_rm(bot, word)
                await admin_channel.send("Le mot « " + word + " » n'est plus censuré sur le serveur")
            else:
                await admin_channel.send("Le mot « " + word + " » n'était pas censuré.")
    else:
        await admin_channel.send("Une demande de suppression de mot à la liste noire a été faite mais aucun mot n'a été spécifié.")


async def blacklist_help(bot):
    admin_channel = bot.get_channel(CHANNEL_LOG)
    await admin_channel.send('''Voici les commandes pour modérateurs :
    ```
    * add mot1 [mot2, ..., motN]\t\t: ajoute de 1 à N mot à la liste de censure.
    * rm mot1 [mot2, ..., motN]\t\t : retire de 1 à N mot à la liste de censure.
    * display \t\t\t\t\t\t  : affiche la liste des mots censurés.
    * show-mute \t\t\t\t\t\t: affiche la liste des personnes en sourdine.
    * top-users [number] \t\t\t   : affiche les utilisateurs les plus grossiers. Opt : choisis combien d'utilisateur afficher.
    * top-words [number] \t\t\t   : affiche les mots plus censurés. Opt : choisis combien en afficher.
    * help\t\t\t\t\t\t\t  : affiche ce message```
    Chaque commande doit être précédé par une mention de ce bot.''')


"""
"   Check if message has any word in the blacklist and censor it.
"   @arg1 : object - discord object to use the discord API
"   @arg3 : object - message object from the discord API
"   @return : nothing
"""
async def check_swearword(bot, message):
    # handle messages sent by moderators to add admin tools in CLI style
    if "🦊Modération" in [role.name for role in message.author.roles] or message.author.guild_permissions.administrator:
        sentence_words = message.content.split()
        if bot.user.mention in sentence_words:
            if len(sentence_words) == 1:
                return
            if sentence_words[1] == "display":
                #db.blacklist_db_display_all_table(bot)
                await blacklist_display(bot)
                return
            elif sentence_words[1] == "add":
                await blacklist_add(bot, message.content)
                return
            elif sentence_words[1] == "rm":
                await blacklist_rm(bot, message.content)
                return
            elif sentence_words[1] == "show-mute":
                await db.blacklist_db_display_mute_users(bot)
            elif sentence_words[1] == "top-users":
                if len(sentence_words) == 3:
                    number=sentence_words[2]
                else:
                    number = "5"
                await db.blacklist_db_display_top_users(bot, number)
            elif sentence_words[1] == "top-words":
                if len(sentence_words) == 3:
                    number = sentence_words[2]
                else:
                    number = "5"
                await db.blacklist_db_display_top_words(bot, number)
            elif sentence_words[1] == "help":
                await blacklist_help(bot)
                return
                

    # check if words of the blacklist is found in the message content
    sentence = message.content.lower()
    chars = ['\.', '\,', '\?', '\;', '\:', '\/', '\§', '\!', '\'', '\"', '\*', '\<', '\>', '\~', '\*', '\`', '_']
    sentence = re.sub("|".join(chars), "", sentence)
    forbidden_word_found = [word for word in blacklist if word in sentence.split()]


    if forbidden_word_found:
        log.log("(on_message) - message with a forbidden word : " + message.content)

        origin_channel = message.channel
        admin_channel = bot.get_channel(CHANNEL_LOG)
        dm_channel = await message.author.create_dm()

        # censor and warn publicly the user
        await message.delete()
        await origin_channel.send(embed = embed.public_swear_censor(message.author.name))

        # put words found into a « printable » string
        words_str=""
        for word in forbidden_word_found:
            await db.blacklist_db_trigger(bot, message.author, word)
            words_str = words_str + " « " + word + " »,"
        words_str = words_str[:-1] # remove the last coma

        # send deleted message to both user & admins
        # adapt sentence if one or multiple words
        title = "Attention " + message.author.name + " !"
        warning_sentence = "L'emploi d'un français châtié est requis sur ce serveur.\nIl est interdit d'utiliser "
        if len(forbidden_word_found) == 1:
            warning_sentence += "le mot"
        else:
            warning_sentence += "les mots" 
        warning_sentence += words_str + ".\nVoici ton message :"

        await dm_channel.send(embed = embed.private_swear_censor(title, warning_sentence))
        await admin_channel.send(embed = embed.private_swear_censor(title, warning_sentence))

        if 2000 < len(message.content):
            with open("message.txt", "w") as file:
                file.write(message.content)
            with open("message.txt", "rb") as file:
                await dm_channel.send("", file=discord.File(file, "MessageContent.txt"))
            with open("message.txt", "rb") as file:
                await admin_channel.send("", file=discord.File(file, "MessageContent.txt"))
            os.remove("message.txt")
        else:
            await dm_channel.send(message.content)
            await admin_channel.send(message.content)
        

        # finally put user in goulg if necessary
        await db.blacklist_db_punish(bot, message.author, message.channel)
