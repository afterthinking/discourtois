# libraries
import discord
import os

import log
import moderator
import database as db


from discord.ext import commands
from dotenv import load_dotenv


# vars
load_dotenv()
token=os.getenv("TOKEN")
LOG_DIR=os.getenv("LOG_DIR")
CHANNEL_LOG=int(os.getenv("CHANNEL_LOG"))

intents = discord.Intents.all()
print(intents)


bot = commands.Bot(command_prefix=".", intents=intents)


"""
" Init function.
" Load the blacklist and tell admins the bot is not active and log bot informations
"""
@bot.event
async def on_ready():

    if moderator.blacklist:
        return

    log.log("Discourtois is starting.")
    log.log("Bot name : " + bot.user.name)
    log.log("Bot ID : " + str(bot.user.id))


    # Load blacklist
    moderator.load_blacklist()
    log.log("Blacklist is loaded")

    # Tell admins the bot is starting 
    admin_channel = bot.get_channel(CHANNEL_LOG)
    al_mess="L'outil *discourtois* est désormais actif"
    log.log(al_mess)
    await admin_channel.send(al_mess)

    # we start the daily even to reduce warns
    db.blacklist_db_start_reduce_warn(bot)



"""
" Check all messages if a word is within the blacklist
"
"   @arg1: message received by the API
"""
@bot.event
async def on_message(message):

    if message.author.id == bot.user.id or message.author.bot:
        return
    
    # check message for swearwords
    await moderator.check_swearword(bot, message)


@bot.event
async def on_message_edit(before, after):

    if before.author.id == bot.user.id:
        return

    await moderator.check_swearword(bot, after)





bot.run(token)

