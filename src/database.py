import os
import datetime
import sqlite3
import asyncio
import threading

import log

from dotenv import load_dotenv

load_dotenv()

blacklist_db=str(os.getenv("BLACKLIST"))
SERVER_GUILD=int(os.getenv("SERVER_GUILD"))
CHANNEL_LOG=int(os.getenv("CHANNEL_LOG"))
GOULAG_ID=int(os.getenv("GOULAG_ID"))




"""
"   Create the database if it doesn't exists
"   @arg    : none
"   @return : nothing
"""
def db_create():
    log.log("Creating database...")
    co = sqlite3.connect(blacklist_db)
    
    """
    "   word : word forbiden
    "   trigger_count : nb of time the word was censored
    """
    co.execute('''	CREATE TABLE IF NOT EXISTS blacklist (
                    id_blacklist	INTEGER 	PRIMARY KEY 	AUTOINCREMENT,
                    word			TEXT					    NOT NULL,
                    trigger_count   INTEGER     DEFAULT 0       NOT NULL);''')
    log.log("blacklist table created")


    """
    "   trigger_count : nb of time the user triggered the bot 
    "   warning_level : set the punition level for the user, decrease with time
    """
    co.execute('''  CREATE TABLE IF NOT EXISTS user (
                    id_user         INTEGER     PRIMARY KEY     NOT NULL,
                    name            TEXT                        NOT NULL,
                    trigger_count   INTEGER     DEFAULT 1       NOT NULL,
                    warning_level   INTEGER     DEFAULT 1       NOT NULL);''')
    log.log("user table created")

    """
    "   date_start  :   exact datetime when user was muted
    "   duration    :   in seconds 
    """
    co.execute('''	CREATE TABLE IF NOT EXISTS mute (
                    id_mute		INTEGER     PRIMARY KEY	    AUTOINCREMENT,
                    id_user		TEXT				        NOT NULL,
                    date_start  TEXT				        NOT NULL,
                    duration    INTEGER					    NOT NULL,
                    FOREIGN KEY(id_user) REFERENCES user(id_user));''')
    log.log("mute table created")

    co.commit()
    co.close()
    log.log("database is created.")


"""
"   Retrieve all the word in the blacklist
"   @arg    : none
"   @return : array of string
"""
def blacklist_db_get():
    log.log("blacklist_db_get function.")
    co = sqlite3.connect(blacklist_db)
    cursor = co.cursor()
    cursor.execute("SELECT word FROM blacklist ORDER BY word ASC;")
    bList = cursor.fetchall()
    co.close()
    return bList

"""
"   Add a word to the blacklist
"   @arg2 : string, the word to ban
"   @return : nothing
"""
async def blacklist_db_add(bot, word):
    admin_channel = bot.get_channel(CHANNEL_LOG)

    co = sqlite3.connect(blacklist_db)
    cursor = co.cursor()
    try:
        cursor.execute(f"INSERT INTO blacklist (word) VALUES ('{word}');")
        co.commit()
        log.log(f"Le mot {word} a été ajouté à la base de donnée.")
    except sqlite3.Error as er:
        log.log('SQLite error: %s' % (' '.join(er.args)))
        log.log("Exception class is: ", er.__class__)
        await admin_channel.send("Le mot « " + word + " » n'a pas pu être ajouté à la liste noire : ")

    co.close()


"""
"   Remove a word to the blacklist
"   @arg2 : string, the word to allow again
"   @return : nothing
"""
async def blacklist_db_rm(bot, word):
    admin_channel = bot.get_channel(CHANNEL_LOG)

    co = sqlite3.connect(blacklist_db)
    cursor = co.cursor()
    try:
        cursor.execute(f"DELETE FROM blacklist WHERE word = '{word}';")
        co.commit()
        log.log(f"Le mot {word} a été supprimé de la base de donnée.")
    except sqlite3.Error as er:
        log.log('SQLite error: %s' % (' '.join(er.args)))
        log.log("Exception class is: ", er.__class__)
        await admin_channel.send("Le mot « " + word + " » n'était pas censuré.")
    co.close()



"""
"   Update db to count and log warnings
"   @arg2 : object - USER object from the discord API
"   @arg3 : String - Word censored by the bot
"   @return : nothing
"""
async def blacklist_db_trigger(bot, user, word):
    log.log("Trigger - updating database")
    co = sqlite3.connect(blacklist_db)
    cursor = co.cursor()
    try:
        # increment count of the word
        cursor.execute(f"UPDATE blacklist SET trigger_count=trigger_count+1 WHERE word = '{word}';")
        co.commit()
    except sqlite3.Error as er:
        admin_channel = bot.get_channel(CHANNEL_LOG)
        log.log(f"SQLite error: {er.args}")
        log.log(f"Exception class is: {er.__class__}")
        await admin_channel.send("Erreur dans la mise à jour des compteurs sur « " + word + " » et « " + user.name + " ».")

    co.close()


"""
"   Put in sleep a process and remove the user from goulag after X seconds
"   @arg2   : USER object from discord API
"   @arg3   : int, number of seconds we mute the user
"   @return : nothing
"""
async def blacklist_db_remove_mute(bot, user, seconds, channel):
    log.log("enter into remove mute")
    log.log(f"User {user.name} is mute for {seconds} seconds. Task going to sleept now...")

    admin_channel = bot.get_channel(CHANNEL_LOG)

    await asyncio.sleep(seconds)

    log.log(f"Remove mute for use {user.name}")
    goulag = bot.get_guild(SERVER_GUILD).get_role(GOULAG_ID)
    await user.remove_roles(goulag)

    co = sqlite3.connect(blacklist_db)
    cursor = co.cursor()
    try:
        cursor.execute(f"SELECT id_user FROM mute WHERE id_user = '{user.id}';")
        data = cursor.fetchall()
        if data:
            cursor.execute(f"DELETE FROM mute WHERE id_user = '{user.id}';")
            co.commit()
            dm_channel = await user.create_dm()
            await channel.send(f"{user.mention} n'est plus en sourdine")
            await admin_channel.send(f"L'utilisateur {user.mention} n'est plus en sourdine")
            await dm_channel.send(f"Vous n'êtes plus en sourdine.")
        else:
            await admin_channel.send(f"Erreur : L'utilisateur « {user.mention} » n'est pas dans la liste des personnes mute.")

    except sqlite3.Error as er:
        log.log(f"SQLite error: {er.args}")
        log.log(f"Exception class is: {er.__class__}")
        await admin_channel.send("Erreur dans la suppression de « " + user.name + " » du goulag.")



"""
"   Update db to count and log everything related to the user. Start a "thread" on _remove_mute with the correct number of seconds
"   @arg2 : object - USER object from the discord API
"   @return : nothing
"""
async def blacklist_db_punish(bot, user, channel):
    log.log("Punish - updating database")
    admin_channel = bot.get_channel(CHANNEL_LOG)
    co = sqlite3.connect(blacklist_db)
    cursor = co.cursor()
    try:
        # step 1 : increment count for the user
        # check if user already exists
        log.log("update or set trigger count and warning_level")
        cursor.execute(f"SELECT warning_level FROM user WHERE id_user = '{user.id}';")
        warn_level = cursor.fetchall()
        if warn_level:
            cursor.execute(f"UPDATE user SET trigger_count=trigger_count+1, warning_level=warning_level+1 WHERE id_user = '{user.id}';")
            co.commit()
            warn_level = warn_level[0][0]
        else:
            cursor.execute(f"INSERT INTO user(id_user, name) VALUES ('{user.id}', '{user.name}');")
            co.commit()
            warn_level=0

        # step 2 : put the user in the mute list.
        # check if user already exists
        if warn_level > 0:
            log.log("update or set duration")
            cursor.execute(f"SELECT duration FROM mute WHERE id_user = '{user.id}';")
            data = cursor.fetchall()
            duration=pow(2,(warn_level-1))*60
            log.log(f"duration of mute for {user.name} ; {user.id} is : {duration} seconds")

            dm_channel = await user.create_dm()
            await admin_channel.send(f"L'utilisateur {user.mention} est au goulag pendant {duration} secondes.")
            await channel.send(f"{user.mention} est au goulag pendant {duration} secondes.")
            await dm_channel.send(f"Vous êtes en sourdine pendant {duration} secondes.")

            if data:
                cursor.execute(f"UPDATE mute SET duration = {duration}, date_start = datetime() WHERE id_user = '{user.id}';")
                co.commit()
            else:
                cursor.execute(f"INSERT INTO mute(id_user, date_start, duration) VALUES ('{user.id}', datetime(), {duration});")
                co.commit()
            # Set goulag role to user
            log.log("goulag role set")
            goulag = bot.get_guild(SERVER_GUILD).get_role(GOULAG_ID)
            await user.add_roles(goulag)

            log.log("task remove_mute for user start")
            # start a task to remove the mute after the duration period
            goulag_over = await asyncio.create_task(blacklist_db_remove_mute(bot, user, duration, channel))

    except sqlite3.Error as er:
        log.log(f"SQLite error: {er.args}")
        log.log(f"Exception class is: {er.__class__}")
        await admin_channel.send("Erreur dans la mise à jour des compteurs pour « " + user.name + " ».")

    co.close()


"""
"   Reduce WARNING_LEVEL for all users once a day
"   @arg2 : threading event to call it everyday
"   @return : nothing
"""
def blacklist_db_reduce_warn(bot, f_stop):
    log.log("REDUCING_WARNING_LEVEL")

    # blacklist_db_display_all_table(bot)

    co = sqlite3.connect(blacklist_db)
    cursor = co.cursor()
    try:
        cursor.execute(f"UPDATE user SET warning_level = case when warning_level > 0 then warning_level-1 else 0 end;")
        co.commit()
    except sqlite3.Error as er:
        admin_channel = bot.get_channel(CHANNEL_LOG)
        log.log(f"SQLite error: {er.args}")
        log.log(f"Exception class is: {er.__class__}")
        log.log("Error updating the warning_level for all users.")
    co.close()

    if not f_stop.is_set():
        # every 12 hours
        threading.Timer(43200, blacklist_db_reduce_warn, [bot, f_stop]).start()


def blacklist_db_start_reduce_warn(bot):
    f_stop = threading.Event()
    # start calling f now and every 60 sec thereafter
    blacklist_db_reduce_warn(bot, f_stop)
    # stop the thread when needed
    #f_stop.set()


async def blacklist_db_display_mute_users(bot):
    admin_channel = bot.get_channel(CHANNEL_LOG)
    co = sqlite3.connect(blacklist_db)
    cursor = co.cursor()
    cursor.execute("SELECT user.id_user, date_start, duration, name FROM user, mute where user.id_user = mute.id_user ORDER BY duration DESC, name ASC;")
    user_data = cursor.fetchall()
    if user_data:
        for user in user_data:
            discord_user = bot.get_user(user[0])
            await admin_channel.send(f"L'utilisateur {discord_user.mention} est au goulag pendant {user[2]} secondes depuis {user[1]}.")
    else:
        await admin_channel.send(f"Aucun utilisateur n'est actuellement en sourdine.")
    co.close()


async def blacklist_db_display_top_users(bot, number):
    admin_channel = bot.get_channel(CHANNEL_LOG)
    if number.isdigit():
        limit="LIMIT " + number
    else:
        limit=""
    co = sqlite3.connect(blacklist_db)
    cursor = co.cursor()
    cursor.execute(f"SELECT id_user, trigger_count, warning_level, name FROM user ORDER BY trigger_count DESC, name ASC {limit};")
    user_data = cursor.fetchall()
    if user_data:
        await admin_channel.send(f"Voici le top 10 des utilisateurs les plus grossiers :")
        for user in user_data:
            discord_user = bot.get_user(user[0])
            await admin_channel.send(f"- {discord_user.mention} : {user[1]} mots prohibés. {user[2]} avertissements en cours")

    else:
        await admin_channel.send(f"Aucun utilisateur n'a encore usé de grossierté.")
    co.close()


async def blacklist_db_display_top_words(bot, number):
    admin_channel = bot.get_channel(CHANNEL_LOG)
    if number.isdigit():
        limit="LIMIT " + number
    else:
        limit=""
    co = sqlite3.connect(blacklist_db)
    cursor = co.cursor()
    cursor.execute(f"SELECT word, trigger_count FROM blacklist ORDER BY trigger_count DESC, word ASC {limit};")
    words = cursor.fetchall()
    if words:
        await admin_channel.send(f"Voici la liste des mots les plus censurés :")
        for word in words:
            await admin_channel.send(f"- {word[0]} : {word[1]}")
    else:
        await admin_channel.send(f"Aucun mot n'est actuellement prohibé.")
    co.close()


"""
"   Few functions to help debug the program
"   TODO : comment or remove the functions
"""
def blacklist_db_show_blacklist(bot):
    co = sqlite3.connect(blacklist_db)
    cursor = co.cursor()
    cursor.execute("SELECT * FROM blacklist;")
    bl_data = cursor.fetchall()
    co.close()
    print(bl_data) 

def blacklist_db_show_users(bot):
    co = sqlite3.connect(blacklist_db)
    cursor = co.cursor()
    cursor.execute("SELECT * FROM user ORDER BY name ASC;")
    user_data = cursor.fetchall()
    co.close()
    print(user_data) 

def blacklist_db_show_mute(bot):
    co = sqlite3.connect(blacklist_db)
    cursor = co.cursor()
    cursor.execute("SELECT * FROM mute;")
    mute_data = cursor.fetchall()
    print(mute_data)
    co.close()

  
def blacklist_db_display_all_table(bot):
    blacklist_db_show_blacklist(bot)
    blacklist_db_show_users(bot)
    blacklist_db_show_mute(bot)

