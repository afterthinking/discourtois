# DISCOURTOIS

Discourtois is a discord bot that censors any message with a forbidden word via a blacklist. 
The bot sends the message back to the user, telling them which word is problematic. 

The bot also posts the message in a dedicated channel so that the moderators are warned.
This bot allows, via a CLI, to display the forbidden words, add and remove words from the blacklist

## Installation

Go to the install directory of your choice then 
```
git clone https://gitlab.com/afterthinking/discourtois
```
Make sure the user:group permissions are good. 

move to the directory of the project :
```
cd discourtois
```

Create a virtual environement if necessary : 
```
python -m venv ./venv
```
activate it :
```
source venv/bin/activate
```

Install the requirements listed in the file *requirements.txt* : 
```
pip install -r requirements.txt
```

Create a file `.env` and add the following informations : 
```
TOKEN=<BOT_TOKEN>
SERVER_GUILD=<SERVER_GUILD>
CHANNEL_LOG=<CHANNEL_ID>
GOULAG_ID=<ROLE_ID>
LOG_DIR=<PATH_TO_LOG>
BLACKLIST=<PATH_TO_BLACKLIST_DB_FILE>


```
Replace the <VARIABLES> with the correct data : 
- TOKEN : your bot TOKEN provided by the discord API 
- SERVER_GUILD : ID of your server
- CHANNEL_LOG : the channel where the bot will display logs for the admins (bot is running, deleted message, etc.)
- GOULAG_ID : role ID that restrict user rights
- LOG_DIR : path to the log directory for the bot (server side) (has to end with the '/') 
- BLACKLIST : path to the blacklist file

Create the log directory : 
```
mkdir log
```


Everything is configured ! you can now run your bot : 
```
python src/main.py & 
```


## Usage

```
# print help manual
@bot help

# print all forbidden words 
@bot display

# add words to the blacklist
@bot add word1 [word2, ..., wordN]

# remove words from the blacklist
@bot rm word1 [word2, ..., wordN]

# display all users currently in mute
@bot show-mute

# display all users or [number] users by trigger (DESC)
@bot top-users [number]

# display all words or [number] words by trigger (DESC)
@bot top-words [number]
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[GNU GPL V3](https://www.gnu.org/licenses/gpl-3.0.en.html)
